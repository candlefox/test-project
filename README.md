Test Project
======================

## Project brief
Convert the following designs to HTML/CSS/JS. Copy this repo to your own GitHub/GitLab account and send us back the link of your repo when you are done.

## Requirements
1. Use HTML5 / SCSS
2. Do not use css framework e.g. bootstrap
3. Make it responsive using your best judgement e.g. consider long text, taller image scenarios & etc.
4. Use Mobile first approach to structure the css
5. Create some animation for the Open/Collapse section(jQuery is fine).

## Design

### Mobile Page
![Mobile Page Design](Mobile.png)
### Desktop Page
![Desktop Page Design](Desktop.png)

### Open/Collapse Section - Mobile
![Mobile - Openned](Mobile-Open.png)
### Open/Collapse Section - Desktop
![Desktop - Openned](Desktop-Open.png)

### Tooltip Section
![Tooltip Design](Tooltip.jpg)

## Supported browsers
Ensure that the elements work and display correctly in the following browsers:

- Firefox (latest version)
- Google Chrome (latest version)
- Microsoft Edge
- Internet Explorer 11

## Coding Standards
When working on the project use consistent coding style. Try to follow WordPress Coding Standards:
- https://make.wordpress.org/core/handbook/best-practices/coding-standards/html/
- https://make.wordpress.org/core/handbook/best-practices/coding-standards/css/
- https://make.wordpress.org/core/handbook/best-practices/coding-standards/javascript/

## Project Deadline
Take your time but try to deliver it within 1 week time.

## Quality Assurance

What you need to do to get high QA score? Simply answer **Yes** to all these questions:

### General

- Are all requirements set above met?
- Is the page working without any JS errors?

### Precision

- Is reasonable precision achieved?

### Browser check

- Does page display and work correctly in supported browsers?

### Valid HTML

- Is the page valid?

### Semantic Markup

- Are the correct tags being used?

### Coding Standards

- Is the page using a consistent HTML coding style?
- Is the page using a consistent CSS coding style?
- Is the page using a consistent JS coding style?